﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RisikoUI.Utility
{
    static class TextRenderer
    {

        public static System.Drawing.Bitmap GetBitmap
            (
                string input,
                System.Drawing.Font inputFont,
                System.Drawing.Color bmpForeground,
                System.Drawing.Color bmpBackground
            )
        {
            System.Drawing.Image bmpText = new System.Drawing.Bitmap(1, 1);
            try
            {
                // Create a graphics object from the image.
                System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmpText);

                // Measure the size of the text when applied to image.
                System.Drawing.SizeF inputSize = g.MeasureString(input, inputFont);

                // Create a new bitmap with the size of the text.
                bmpText = new System.Drawing.Bitmap((int)inputSize.Width,
                    (int)inputSize.Height);

                // Instantiate graphics object, again, since our bitmap
                // was modified.
                g = System.Drawing.Graphics.FromImage(bmpText);

                // Draw a background to the image.
                g.FillRectangle(new System.Drawing.Pen(bmpBackground).Brush,
                    new System.Drawing.Rectangle(0, 0,
                    Convert.ToInt32(inputSize.Width),
                    Convert.ToInt32(inputSize.Height)));

                // Draw the text to the image.
                g.DrawString(input, inputFont,
                    new System.Drawing.Pen(bmpForeground).Brush, new System.Drawing.PointF(0, 0));
            }
            catch
            {
                // Draw a blank image with background.
                System.Drawing.Graphics.FromImage(bmpText).FillRectangle(
                    new System.Drawing.Pen(bmpBackground).Brush,
                    new System.Drawing.Rectangle(0, 0, 1, 1));
            }

            return (System.Drawing.Bitmap)bmpText;
        }

        public static Texture2D GetTexture2DFromBitmap(GraphicsDevice device, System.Drawing.Bitmap bitmap)
        {
            Texture2D tex = new Texture2D(device, bitmap.Width, bitmap.Height);

            System.Drawing.Imaging.BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

            int bufferSize = data.Height * data.Stride;

            //create data buffer 
            byte[] bytes = new byte[bufferSize];

            // copy bitmap data into buffer
            Marshal.Copy(data.Scan0, bytes, 0, bytes.Length);

            // copy our buffer to the texture
            tex.SetData(bytes);

            // unlock the bitmap data
            bitmap.UnlockBits(data);

            return tex;
        }

    }
}
