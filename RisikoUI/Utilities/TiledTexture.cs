﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RisikoUI.Utility
{
    class TiledTexture
    {
        //importa il file della texture
        //importa il file info sulla texture
        //il file info avra il seguente contenuto:
        //nome_tile: x,y,w,h
        //per ogni tile c'e' una riga

        public Texture2D Texture { get; private set; }
        private Dictionary<string, Rectangle> Tiles;

        public TiledTexture(ContentManager Content, string tile)
        {
            Texture = Content.Load<Texture2D>(tile);
            ReadTileFile(Path.Combine(Content.RootDirectory, tile + ".tst"));
        }

        private void ReadTileFile(string tilefile)
        {
            var file = new StreamReader(tilefile);
            string line;
            Tiles = new Dictionary<string, Rectangle>();
            while ((line = file.ReadLine()) != null)
            {
                var oo = line.Split(':');
                if (oo.Length != 2) throw new Exception("Incorrect file syntax: NAME:RECTANGLE");
                var name = oo[0].Trim();
                var rt = oo[1].Split(',');
                if (rt.Length != 4) throw new Exception("Incorrect file syntax: X,Y,W,H");
                int x, y, w, h;

                if (!Int32.TryParse(rt[0].Trim(), out x))
                    throw new Exception("Incorrect file syntax: X");
                if (!Int32.TryParse(rt[1].Trim(), out y))
                    throw new Exception("Incorrect file syntax: Y");
                if (!Int32.TryParse(rt[2].Trim(), out w))
                    throw new Exception("Incorrect file syntax: W");
                if (!Int32.TryParse(rt[3].Trim(), out h))
                    throw new Exception("Incorrect file syntax: H");

                if (!Tiles.ContainsKey(name))
                {
                    Tiles.Add(name, new Rectangle(x, y, w, h));
                }
                else
                {
                    throw new Exception("Equal name tiles not allowed");
                }

            }
            file.Close();
        }

        public Rectangle GetTile(string name)
        {
            if (!Tiles.ContainsKey(name)) throw new Exception(String.Format("Tile {0} does not exists", name));
            else return Tiles[name];
        }

        public Point GetTileSize(string name)
        {
            if (!Tiles.ContainsKey(name)) throw new Exception(String.Format("Tile {0} does not exists", name));
            else return Tiles[name].Size;
        }

        public static RenderTarget2D GenerateFullTexture(GraphicsDevice g, TiledTexture tt, Rectangle Size)
        {
            var res = new RenderTarget2D(g, Size.Width, Size.Height);

            g.SetRenderTarget(res);
            g.Clear(Color.Transparent);
            SpriteBatch sb = new SpriteBatch(g);
            sb.Begin();
            Rectangle angle = tt.GetTile("angle");
            Rectangle side = tt.GetTile("side");
            Rectangle inside = tt.GetTile("inside");

            int x = 0;
            int y = 0;

            int nTopTiles = (Size.Width - (tt.GetTile("angle").Width * 2)) / (tt.GetTile("side").Width);
            int nSideTiles = (Size.Height - (tt.GetTile("angle").Height * 2)) / (tt.GetTile("side").Height);
            int remainTop = (Size.Width - (tt.GetTile("angle").Width * 2)) % (tt.GetTile("side").Width);
            int remainSide = (Size.Height - (tt.GetTile("angle").Height * 2)) % (tt.GetTile("side").Height);
            int w = 40;
            int h = 40;

            sb.Draw(tt.Texture, new Rectangle(x, y, w, h), angle, Color.White);
            for (int i = 0; i < nTopTiles; i++)
            {
                x += w;
                sb.Draw(tt.Texture, new Rectangle(x, y, w, h), side, Color.White);
            }
            x += w;
            if (remainTop > 0)
            {
                sb.Draw(tt.Texture, new Rectangle(x, y, remainTop, h), side, Color.White);
                x += remainTop;
            }
            sb.Draw(tt.Texture, new Rectangle(x, y, w, h), angle, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);

            for (int i = 0; i < nSideTiles; i++)
            {
                x = 0;
                y += h;
                sb.Draw(tt.Texture, new Rectangle(x, y, w, h), side, Color.White, MathHelper.PiOver2 * 3.0f, new Vector2(w, 0), SpriteEffects.None, 0);
                for (int j = 0; j < nTopTiles; j++)
                {
                    x += w;
                    sb.Draw(tt.Texture, new Rectangle(x, y, w, h), inside, Color.White);
                }
                x += w;
                if (remainTop > 0)
                {
                    sb.Draw(tt.Texture, new Rectangle(x, y, remainTop, h), inside, Color.White);
                    x += remainTop;
                }
                sb.Draw(tt.Texture, new Rectangle(x, y, w, h), side, Color.White, MathHelper.PiOver2, new Vector2(0, h), SpriteEffects.None, 0);
            }
            y += h;
            x = 0;

            sb.Draw(tt.Texture, new Rectangle(x, y, w, h), angle, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipVertically, 0);
            for (int i = 0; i < nTopTiles; i++)
            {
                x += w;
                sb.Draw(tt.Texture, new Rectangle(x, y, w, h), side, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipVertically, 0);
            }
            x += w;
            if (remainTop > 0)
            {
                sb.Draw(tt.Texture, new Rectangle(x, y, remainTop, h), side, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipVertically, 0);
                x += remainTop;
            }
            sb.Draw(tt.Texture, new Rectangle(x, y, w, h), angle, Color.White, MathHelper.PiOver2, new Vector2(0, h), SpriteEffects.FlipHorizontally, 0);

            sb.End();
            g.SetRenderTarget(null);

            return res;

        }




    }
}
