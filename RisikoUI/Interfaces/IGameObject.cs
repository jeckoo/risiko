﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RisikoUI.Interfaces
{
    interface IGameObject
    {
        void Load(ContentManager Content);
        void Initialize(GraphicsDevice g);
        void Update(GameTime gameTime);
        void Draw(GameTime gameTime, SpriteBatch sb);
    }
}
