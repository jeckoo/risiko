﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RisikoUI.Interfaces
{
    interface IArea
    {
        event EventHandler Enter;
        event EventHandler Hover;
        event EventHandler Exit;

        void OnEntering(EventArgs e);
        void OnExiting(EventArgs e);
        void OnHovering(EventArgs e);

    }
}
