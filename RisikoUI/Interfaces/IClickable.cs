﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RisikoUI.Interfaces
{
    interface IClickable : IArea
    {
        event EventHandler Click;

        void OnClick(EventArgs e);
    }
}
