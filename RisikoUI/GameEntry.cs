﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RisikoUI
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameEntry : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D board;

        Random rnd = new Random();

        //static float lastXMASUpdate = 0;
        bool mheld = false;

        //public static RisikoCore.PlayerColor MyColor = RisikoCore.PlayerColor.RED;
        public static bool MyTurn = true;
        //public static List<TerritoryInfo> Territories;

        public GameEntry()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1728;
            graphics.PreferredBackBufferHeight = 972;
            IsMouseVisible = true;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            //Territories = new List<TerritoryInfo>();
            //foreach (var t in TerritoryInfo.TERRITORIES)
            {
                //var tinfo = new TerritoryInfo();
                //tinfo.Radius = 50;
                //tinfo.Center = new Vector2(t.x, t.y);
                //tinfo.Initialize(graphics.GraphicsDevice);
                //tinfo.Name = t.name;
                //tinfo.Owner = (RisikoCore.PlayerColor)rnd.Next(0, 6);
                //Territories.Add(tinfo);


            }
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            board = Content.Load<Texture2D>("RisikoBoard");





            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                mheld = true;

            if (Mouse.GetState().LeftButton == ButtonState.Released && mheld)
            {
                OnClick(gameTime);
                mheld = false;
            }








            //Territories[0].armycount = 100;

            /* 
                lastXMASUpdate += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (lastXMASUpdate > 500)
                {
                    foreach (var t in Territories)
                    {
                        t.Owner = (RisikoCore.PlayerColor)rnd.Next(0, 6);
                        t.armycount = rnd.Next(1, 38);
                    }
                    lastXMASUpdate = 0;
                
                }
            */

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            spriteBatch.Draw(board, new Rectangle(0, 0, 1728, 972), Color.White);

            //foreach (var info in Territories)
           // {
                //info.Owner = RisikoCore.PlayerColor.RED;
             //   info.Draw(gameTime, spriteBatch);
           // }
            spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }


        public void OnClick(GameTime gameTime)
        {
            //foreach (var t in Territories)
            //{
            //    if (t.Contains(Mouse.GetState().Position))
            //    {
            //        t.OnClick(gameTime);
            //    }
            //}
        }

    }
}
