﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RisikoUI.Utility;

namespace RisikoUI.Controls
{
    class TiledButton : Button
    {
        RenderTarget2D FullTexture;
        TiledTexture texture;
        Rectangle Size;
        private int ScaleX;
        private int ScaleY;


        public TiledButton(Rectangle hitbox)
        {
            HitBox = hitbox;
        }



        public override void Load(ContentManager Content)
        {
            texture = new TiledTexture(Content, "BTile");
        }

        public override void Initialize(GraphicsDevice g)
        {
            base.Initialize(g);
        }

        public override void Update(GameTime gameTime)
        {
            //base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch sb)
        {
            //base.Draw(gameTime, sb);
            sb.Draw(FullTexture, new Vector2(HitBox.X, HitBox.Y), null, Color.White, 0f, Vector2.Zero, new Vector2(1.0f / ScaleX, 1.0f / ScaleY), SpriteEffects.None, 0);
        }




    }
}
