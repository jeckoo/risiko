﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RisikoUI.Interfaces;
using RisikoUI.Utility;

namespace RisikoUI.Controls
{
    class Button : IGameObject, IClickable
    {
        public event EventHandler Click;
        public event EventHandler Enter;
        public event EventHandler Hover;
        public event EventHandler Exit;


        public Rectangle HitBox { get; set; }

        protected Texture2D Texture { get; set; }
        protected Texture2D TextTexture { get; set; }
        protected Rectangle TextPosition { get; set; }

        protected string text;
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                TextTexturesChanged = true;
            }
        }


        protected bool inside = false;
        protected bool pressed = false;
        protected bool TextTexturesLoaded = false;
        protected bool TextTexturesChanged = false;
        protected GraphicsDevice GDevice;

        private void RunEvents()
        {
            var mpos = Mouse.GetState().Position;
            var lb = Mouse.GetState().LeftButton;

            if (HitBox.Contains(mpos))
            {
                if (!inside)
                {
                    inside = true;
                    OnEntering(new EventArgs());
                }
                else
                {
                    OnHovering(new EventArgs());
                }
            }
            else
            {
                if (inside)
                {
                    inside = false;
                    OnExiting(new EventArgs());
                }
            }

            if (inside)
            {
                if (lb == ButtonState.Pressed)
                {
                    pressed = true;
                }
                else
                {
                    if (pressed)
                    {
                        OnClick(new EventArgs());
                    }
                    pressed = false;

                }
            }
            else
            {
                pressed = false;
            }





        }

        public void OnClick(EventArgs e)
        {
            Click?.Invoke(this, e);
        }

        public void OnEntering(EventArgs e)
        {
            Enter?.Invoke(this, e);
        }

        public void OnExiting(EventArgs e)
        {
            Exit?.Invoke(this, e);
        }

        public void OnHovering(EventArgs e)
        {
            Hover?.Invoke(this, e);
        }

        public virtual void Update(GameTime gameTime)
        {
            RunEvents();
        }

        public virtual void Load(ContentManager Content)
        {
            Texture = Content.Load<Texture2D>("Inside");
        }

        public virtual void Initialize(GraphicsDevice g)
        {
            GDevice = g;
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch sb)
        {
            if (inside && !pressed)
            {
                sb.Draw(Texture, HitBox, Color.White);
                sb.Draw(Texture, HitBox, new Color(0, 0, 0, 100));
            }
            else if (inside && pressed)
            {
                sb.Draw(Texture, HitBox, Color.White);
                sb.Draw(Texture, HitBox, new Color(255, 255, 255, 100));
            }
            else
            {
                sb.Draw(Texture, HitBox, Color.White);
            }
        }

        protected virtual void GenerateText()
        {
            var font = new System.Drawing.Font("Arial", 14);
            var color = System.Drawing.Color.FromArgb(255, 255, 255, 255);
            System.Drawing.Bitmap img = TextRenderer.GetBitmap(Text, font, color, System.Drawing.Color.Transparent);
            TextTexture = TextRenderer.GetTexture2DFromBitmap(GDevice, img);

            TextPosition = new Rectangle((int)(HitBox.Center.X) - (TextTexture.Width / 2),
                                 (int)(HitBox.Center.Y) - (TextTexture.Height / 2),
                                 TextTexture.Width,
                                 TextTexture.Height);
        }

    }
}
